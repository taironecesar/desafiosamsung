package br.com.taironecesar.projects.samsung.inventoryapplication.task;

import android.os.AsyncTask;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.taironecesar.projects.samsung.inventoryapplication.database.DatabaseClient;
import br.com.taironecesar.projects.samsung.inventoryapplication.model.Equipment;

public class SaveEquipemnt extends AsyncTask<Equipment, Void, Void> {
    private ITaskListener listener;
    private ArrayList<Equipment> equipmentList = new ArrayList<Equipment>();

    public SaveEquipemnt(ITaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected Void doInBackground(Equipment... equipments) {
        Equipment equipment = equipments[0];

        long code = DatabaseClient.getInstance(listener.getListenerContext()).getInventoryDatabase()
                .equipmentDAO()
                .insert(equipment);

        equipment.setCode(code);
        equipmentList.add(equipment);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Toast.makeText(listener.getListenerContext(),"Saved", Toast.LENGTH_LONG).show();
        listener.onSuccess(equipmentList);
    }
}
