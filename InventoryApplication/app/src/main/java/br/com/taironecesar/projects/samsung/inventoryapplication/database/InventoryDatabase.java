package br.com.taironecesar.projects.samsung.inventoryapplication.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import br.com.taironecesar.projects.samsung.inventoryapplication.dao.EquipmentDAO;
import br.com.taironecesar.projects.samsung.inventoryapplication.model.DateTypeConverter;
import br.com.taironecesar.projects.samsung.inventoryapplication.model.Equipment;

@Database(entities = {Equipment.class}, version = 1)
@TypeConverters({DateTypeConverter.class})
public abstract class InventoryDatabase extends RoomDatabase {
    public abstract EquipmentDAO equipmentDAO();
}
