package br.com.taironecesar.projects.samsung.inventoryapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.taironecesar.projects.samsung.inventoryapplication.R;
import br.com.taironecesar.projects.samsung.inventoryapplication.model.Equipment;
import br.com.taironecesar.projects.samsung.inventoryapplication.util.Util;
import br.com.taironecesar.projects.samsung.inventoryapplication.view.EquipmentActivity;

public class EquipmentAdapter extends RecyclerView.Adapter<EquipmentAdapter.EquipmentViewHolder> {

    private Context context;
    private List<Equipment> equipmentList;

    public EquipmentAdapter(Context context, List<Equipment> equipmentList) {
        this.context = context;
        this.equipmentList = equipmentList;
    }

    @Override
    public EquipmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_equipments,
                parent, false);
        return new EquipmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EquipmentViewHolder holder, int position) {
        Equipment equipment = equipmentList.get(position);

        holder.typeTextView.setText(equipment.getType());
        holder.modelTextView.setText(equipment.getModel());
        holder.acquisitionTextView.setText(Util.formattedDate(equipment.getAcquisitionDate()));
        holder.costTextView.setText(equipment.getCost());
    }

    @Override
    public int getItemCount() {
        return equipmentList.size();
    }

    class EquipmentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView typeTextView, modelTextView, acquisitionTextView, costTextView;

        public EquipmentViewHolder(View itemView) {
            super(itemView);
            typeTextView = itemView.findViewById(R.id.typeEditText);
            modelTextView = itemView.findViewById(R.id.modelTextView);
            acquisitionTextView = itemView.findViewById(R.id.acquisitionTextView);
            costTextView = itemView.findViewById(R.id.costTextView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Equipment equipment = equipmentList.get(getAdapterPosition());

            Intent intent = new Intent(context, EquipmentActivity.class);
            intent.putExtra(EquipmentActivity.EQUIPMENT_VARIABLE, equipment);

            context.startActivity(intent);
        }
    }
}
