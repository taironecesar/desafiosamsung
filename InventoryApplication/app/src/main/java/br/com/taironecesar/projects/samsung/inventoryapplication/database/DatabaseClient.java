package br.com.taironecesar.projects.samsung.inventoryapplication.database;

import android.arch.persistence.room.Room;
import android.content.Context;

public class DatabaseClient {
    private static final String DATABASE_NAME = "InventoryApp";

    private Context context;
    private static DatabaseClient INSTANCE;

    private InventoryDatabase inventoryDatabase;

    private DatabaseClient(Context context) {
        this.context = context;

        inventoryDatabase = Room.databaseBuilder(context, InventoryDatabase.class,
                DATABASE_NAME).build();
    }

    public static synchronized DatabaseClient getInstance(Context context) {
        if(INSTANCE == null) {
            INSTANCE = new DatabaseClient(context);
        }
        return INSTANCE;
    }

    public InventoryDatabase getInventoryDatabase() {
        return inventoryDatabase;
    }
}
