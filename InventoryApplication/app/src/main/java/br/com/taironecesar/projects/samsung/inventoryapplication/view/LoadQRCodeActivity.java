package br.com.taironecesar.projects.samsung.inventoryapplication.view;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import br.com.taironecesar.projects.samsung.inventoryapplication.MainActivity;
import br.com.taironecesar.projects.samsung.inventoryapplication.R;

public class LoadQRCodeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_qrcode);

        final Activity activity = this;

        findViewById(R.id.captureImageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(activity);
                intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                intentIntegrator.setPrompt("Find code");
                intentIntegrator.setCameraId(0); // Back camera
                intentIntegrator.initiateScan();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode,
                data);

        if(intentResult != null) {
            if(intentResult.getContents() != null) {
                alert(intentResult.getContents());
            } else {
                alert("Canceled scanning");
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void alert(String message) {
        Intent intent = new Intent();
        intent.putExtra(MainActivity.REQUEST_VARIABLE_QRCODE_CONTENT, message);
        setResult(RESULT_OK, intent);
        finish();
    }
}
