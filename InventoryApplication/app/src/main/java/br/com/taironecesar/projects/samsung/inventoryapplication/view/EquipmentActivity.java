package br.com.taironecesar.projects.samsung.inventoryapplication.view;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import br.com.taironecesar.projects.samsung.inventoryapplication.MainActivity;
import br.com.taironecesar.projects.samsung.inventoryapplication.R;
import br.com.taironecesar.projects.samsung.inventoryapplication.model.Equipment;
import br.com.taironecesar.projects.samsung.inventoryapplication.task.DeleteEquipmentTask;
import br.com.taironecesar.projects.samsung.inventoryapplication.task.ITaskListener;
import br.com.taironecesar.projects.samsung.inventoryapplication.task.SaveEquipemnt;
import br.com.taironecesar.projects.samsung.inventoryapplication.task.UpdateEquipmentTask;
import br.com.taironecesar.projects.samsung.inventoryapplication.util.Util;

public class EquipmentActivity extends AppCompatActivity implements ITaskListener {

    private static final int REQUEST_CODE_CAMERA = 1237;

    private static final int ADD_EQUIPMENT_OPERATION = 0;
    private static final int UPDATE_EQUIPMENT_OPERATION = 1;

    public static final String EQUIPMENT_VARIABLE = "EquipmentVariable";

    private int operation;

    private ImageView photoImageView;
    private String path;

    private EditText typeEditText, modelEditText, acquisitionEditText, costEditText;
    private Button saveButton, deleteButton;

    Calendar calendar = Calendar.getInstance();
    private Equipment currentEquipment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipment);

        typeEditText = findViewById(R.id.typeEditText);
        modelEditText = findViewById(R.id.modelEditText);
        costEditText = findViewById(R.id.costEditText);

        acquisitionEditText = findViewById(R.id.acquisitionEditText);
        initAcquisitionEditText();

        photoImageView = (ImageView) findViewById(R.id.photoImageView);
        photoImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent camera= new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(camera, REQUEST_CODE_CAMERA);
            }
        });

        saveButton = (Button) findViewById(R.id.saveActionButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });

        deleteButton = (Button) findViewById(R.id.deleteActionButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        EquipmentActivity.this);
                builder.setTitle(R.string.confirm_remove_action_label);
                builder.setPositiveButton(R.string.yes_label, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        delete();
                    }
                });
                builder.setNegativeButton(R.string.no_label, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) { }
                });

                AlertDialog ad = builder.create();
                ad.show();
            }
        });

        loadEquipment();
    }

    private void loadEquipment() {
        currentEquipment = (Equipment) getIntent().getSerializableExtra(EQUIPMENT_VARIABLE);

        if(currentEquipment != null) {
            operation = UPDATE_EQUIPMENT_OPERATION;
            setTitle(getResources().getString(R.string.equipment_update_title));
            typeEditText.setText(currentEquipment.getType());
            modelEditText.setText(currentEquipment.getModel());
            acquisitionEditText.setText(Util.formattedDate(currentEquipment.getAcquisitionDate()));
            costEditText.setText(currentEquipment.getCost());

            // Este botão está sendo escondido, pois, revendo a descrição do desafio técnico, notei
            // que não deve haver a função de atualizar o equipamento.
            saveButton.setVisibility(View.GONE);
        } else {
            setTitle(getResources().getString(R.string.equipment_add_title));
            operation = ADD_EQUIPMENT_OPERATION;
            deleteButton.setVisibility(View.GONE);
        }
    }

    private void save() {
        final String typeValue = typeEditText.getText().toString().trim();
        final String modelValue = modelEditText.getText().toString().trim();
        final String acquisitionValue = acquisitionEditText.getText().toString().trim();
        final String costValue = costEditText.getText().toString().trim();

        if(typeValue.isEmpty()) {
            typeEditText.setError(getString(R.string.equipment_type_error));
            typeEditText.requestFocus();
            return;
        }

        if(modelValue.isEmpty()) {
            modelEditText.setError(getString(R.string.equipment_model_error));
            modelEditText.requestFocus();
            return;
        }

        if(acquisitionValue.isEmpty()) {
            acquisitionEditText.setError(getString(R.string.equipment_acquisition_error));
            acquisitionEditText.requestFocus();
            return;
        }

        if(costValue.isEmpty()) {
            costEditText.setError(getString(R.string.equipment_cost_error));
            costEditText.requestFocus();
            return;
        }

        switch (operation) {
            case ADD_EQUIPMENT_OPERATION:
                Equipment equipment = new Equipment();
                equipment.setType(typeValue);
                equipment.setModel(modelValue);
                equipment.setAcquisitionDate(Util.convertStringToDate(acquisitionValue));
                equipment.setCost(costValue);

                SaveEquipemnt saveEquipemnt = new SaveEquipemnt(this);
                saveEquipemnt.execute(equipment);
                break;
            case UPDATE_EQUIPMENT_OPERATION:
                currentEquipment.setType(typeValue);
                currentEquipment.setModel(modelValue);
                currentEquipment.setAcquisitionDate(Util.convertStringToDate(acquisitionValue));
                currentEquipment.setCost(costValue);

                UpdateEquipmentTask updateEquipmentTask = new UpdateEquipmentTask(this);
                updateEquipmentTask.execute(currentEquipment);
        }
    }

    private void delete() {
        DeleteEquipmentTask deleteEquipmentTask = new DeleteEquipmentTask(this);
        deleteEquipmentTask.execute(currentEquipment);
    }

    @Override
    public Context getListenerContext() {
        return this.getApplicationContext();
    }

    @Override
    public void onSuccess(List<Equipment> equipmentList) {
        if(!equipmentList.isEmpty()) {
            Equipment equipment = equipmentList.get(0);

            //persistImage(equipment);

            Intent intent = new Intent();
            intent.putExtra(MainActivity.REQUEST_VARIABLE_MAIN_ACTIVITY, equipment);
            setResult(RESULT_OK, intent);
        }

        this.finish();
    }

    @Override
    public void onError() {

    }

    private void persistImage(Equipment equipment) {
        BitmapDrawable draw = (BitmapDrawable) photoImageView.getDrawable();
        Bitmap bitmap = draw.getBitmap();

        String imagePath = MediaStore.Images.Media.insertImage(
                getContentResolver(),
                bitmap,
                Long.toString(equipment.getCode()) + ".jpg",
                equipment.getModel()
        );

        Uri URI = Uri.parse(imagePath);
    }

    private void initAcquisitionEditText() {
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                acquisitionEditText.setText(Util.formattedDate(calendar.getTime()));
            }
        };

        acquisitionEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    new DatePickerDialog(EquipmentActivity.this, date, calendar
                            .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            photoImageView.setImageBitmap(photo);
        }
    }
}
