package br.com.taironecesar.projects.samsung.inventoryapplication.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import br.com.taironecesar.projects.samsung.inventoryapplication.model.Equipment;

@Dao
public interface EquipmentDAO {

    @Query("Select * from equipment")
    List<Equipment> getAll();

    @Insert
    long insert(Equipment equipment);

    @Update
    void update(Equipment equipment);

    @Delete
    void delete(Equipment equipment);
}
