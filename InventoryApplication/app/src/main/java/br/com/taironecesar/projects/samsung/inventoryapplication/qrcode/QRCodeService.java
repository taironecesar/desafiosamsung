package br.com.taironecesar.projects.samsung.inventoryapplication.qrcode;

import android.app.DownloadManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.Console;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import br.com.taironecesar.projects.samsung.inventoryapplication.util.Util;

public class QRCodeService extends IntentService {
    private static final String SERVICE_NAME = "QRCODE_SERVICE";

    public QRCodeService() {
        super(SERVICE_NAME);
    }

    private DownloadManager downloadManager;
    private Long reference;

    @Override
    protected void onHandleIntent(Intent intent) {
//        new DownloadQRCodeImage().execute("http://developer.android.com/images/activity_lifecycle.png");

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse("http://developer.android.com/images/activity_lifecycle.png");
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        reference = downloadManager.enqueue(request);
    }


    class DownloadQRCodeImage extends AsyncTask<String, Void, Bitmap> {

        private Bitmap downloadImageBitmap(String sUrl) {
            Bitmap bitmap = null;
            try {

                final InputStream inputStream = new URL(sUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);

                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
        }

        protected void onPostExecute(Bitmap result) {
            Uri path = Util.saveImage(getApplicationContext(), result, "my_image.png");
            shareQRCode(path);
        }

        private void shareQRCode(Uri path) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/png");
            share.putExtra(Intent.EXTRA_STREAM, path);
            startActivity(Intent.createChooser(share, "Share Image"));
        }
    }
}
