package br.com.taironecesar.projects.samsung.inventoryapplication.qrcode;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;

import br.com.taironecesar.projects.samsung.inventoryapplication.util.Util;

public class DownloadQRCodeImage extends AsyncTask<String, Void, Bitmap> {

    private Bitmap downloadImageBitmap(String sUrl) {
        Bitmap bitmap = null;
        try {
            InputStream inputStream = new URL(sUrl).openStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return downloadImageBitmap(params[0]);
    }

    protected void onPostExecute(Bitmap result) {
        //Util.saveImage(getApplicationContext(), result, "my_image.png");
    }
}