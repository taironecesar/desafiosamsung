package br.com.taironecesar.projects.samsung.inventoryapplication;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentResult;

import java.util.List;

import br.com.taironecesar.projects.samsung.inventoryapplication.adapter.EquipmentAdapter;
import br.com.taironecesar.projects.samsung.inventoryapplication.database.DatabaseClient;
import br.com.taironecesar.projects.samsung.inventoryapplication.model.Equipment;
import br.com.taironecesar.projects.samsung.inventoryapplication.task.GetEquipmentTask;
import br.com.taironecesar.projects.samsung.inventoryapplication.task.ITaskListener;
import br.com.taironecesar.projects.samsung.inventoryapplication.view.EquipmentActivity;
import br.com.taironecesar.projects.samsung.inventoryapplication.view.LoadQRCodeActivity;

public class MainActivity extends AppCompatActivity implements ITaskListener {
    private static final String URL_BASE_QRCODE =
            "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=";

    public static final int REQUEST_CODE_MAIN_ACTIVITY = 1;
    public static final String REQUEST_VARIABLE_MAIN_ACTIVITY = "REQUEST_VARIABLE_MAIN_ACTIVITY";

    public static final int REQUEST_CODE_READ_QRCODE = 2;
    public static final String REQUEST_VARIABLE_QRCODE_CONTENT = "REQUEST_VARIABLE_QRCODE_CONTENT";

    List<Equipment> equipmentList;
    private RecyclerView recyclerView;

    private DownloadManager downloadManager;
    private Long reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        EquipmentActivity.class);
                startActivityForResult(intent, REQUEST_CODE_MAIN_ACTIVITY);
            }
        });

        loadReceiver();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getEquipments();
    }

    private void getEquipments() {
        GetEquipmentTask getEquipments = new GetEquipmentTask(this);
        getEquipments.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_read) {
            Intent intent = new Intent(this, LoadQRCodeActivity.class);
            startActivityForResult(intent, REQUEST_CODE_READ_QRCODE);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Context getListenerContext() {
        return this.getApplicationContext();
    }

    @Override
    public void onSuccess(List<Equipment> equipmentList) {
        this.equipmentList = equipmentList;
        EquipmentAdapter adapter = new EquipmentAdapter(MainActivity.this, this.equipmentList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onError() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_OK) {
            if(requestCode == REQUEST_CODE_MAIN_ACTIVITY) {
                Equipment equipment = (Equipment) data.getSerializableExtra(REQUEST_VARIABLE_MAIN_ACTIVITY);

                downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(URL_BASE_QRCODE + equipment.toString());
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                reference = downloadManager.enqueue(request);

            } else if (requestCode == REQUEST_CODE_READ_QRCODE) {
                String message = data.getStringExtra(REQUEST_VARIABLE_QRCODE_CONTENT);
                String index = message.split(Equipment.SEPARATOR)[0];
                openEquipmentPage(index);
            }
        }
    }

    public void openEquipmentPage(String codeEquipment) {
        Boolean foundEquipment = false;
        for( Equipment equipment : equipmentList) {
            if(Long.toString(equipment.getCode()).equals(codeEquipment)) {
                foundEquipment = true;
                Intent intent = new Intent(this, EquipmentActivity.class);
                intent.putExtra(EquipmentActivity.EQUIPMENT_VARIABLE, equipment);
                startActivity(intent);
            }
        }

        if(!foundEquipment) {
            Toast.makeText(this, getString(R.string.no_found_equipment), Toast.LENGTH_LONG).show();
        }
    }

    private void loadReceiver() {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if(DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    DownloadManager.Query requestQuery = new DownloadManager.Query();
                    requestQuery.setFilterById(reference);

                    Cursor cursor = downloadManager.query(requestQuery);
                    if(cursor.moveToFirst()) {
                        int colunmIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if(DownloadManager.STATUS_SUCCESSFUL == cursor.getInt(colunmIndex)) {
                            String uirString = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));

                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("image/png");
                            share.putExtra(Intent.EXTRA_STREAM, Uri.parse(uirString));
                            startActivity(Intent.createChooser(share, "Share Image"));
                        }
                    }
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }
}
