package br.com.taironecesar.projects.samsung.inventoryapplication.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.io.Serializable;
import java.util.Date;

@Entity
public class Equipment implements Serializable {
    public static final String SEPARATOR = ";";

    @PrimaryKey(autoGenerate = true)
    private long code;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "model")
    private String model;

    @ColumnInfo(name = "acquisitionDate")
    private Date acquisitionDate;

    @ColumnInfo(name = "cost")
    private String cost;


    public long getCode() {
        return code;
    }

    public void setCode(long code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getAcquisitionDate() {
        return acquisitionDate;
    }

    public void setAcquisitionDate(Date acquisitionDate) {
        this.acquisitionDate = acquisitionDate;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return this.code + SEPARATOR + this.type + SEPARATOR + this.model + SEPARATOR +
                this.acquisitionDate + SEPARATOR + this.cost;
    }
}
