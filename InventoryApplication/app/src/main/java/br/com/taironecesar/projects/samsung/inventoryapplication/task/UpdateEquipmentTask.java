package br.com.taironecesar.projects.samsung.inventoryapplication.task;

import android.os.AsyncTask;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.taironecesar.projects.samsung.inventoryapplication.database.DatabaseClient;
import br.com.taironecesar.projects.samsung.inventoryapplication.model.Equipment;

public class UpdateEquipmentTask extends AsyncTask<Equipment, Void, Void> {
    private ITaskListener listener;
    private ArrayList<Equipment> equipmentList = new ArrayList<Equipment>();

    public UpdateEquipmentTask(ITaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected Void doInBackground(Equipment... equipments) {
        Equipment equipment = equipments[0];

        DatabaseClient.getInstance(listener.getListenerContext()).getInventoryDatabase()
            .equipmentDAO()
            .update(equipment);
        equipmentList.add(equipment);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Toast.makeText(listener.getListenerContext(),"Updated", Toast.LENGTH_LONG).show();
        listener.onSuccess(equipmentList);
    }
}
