package br.com.taironecesar.projects.samsung.inventoryapplication.task;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import br.com.taironecesar.projects.samsung.inventoryapplication.database.DatabaseClient;
import br.com.taironecesar.projects.samsung.inventoryapplication.model.Equipment;

public class GetEquipmentTask extends AsyncTask<Void, Void, List<Equipment>> {
    private ITaskListener listener;

    public GetEquipmentTask(ITaskListener listener) {
        this.listener = listener;
    }
    @Override
    protected List<Equipment> doInBackground(Void... voids) {
        List<Equipment> equipmentList = DatabaseClient
                .getInstance(listener.getListenerContext())
                .getInventoryDatabase()
                .equipmentDAO()
                .getAll();
        return equipmentList;
    }

    @Override
    protected void onPostExecute(List<Equipment> equipmentList) {
        super.onPostExecute(equipmentList);
        listener.onSuccess(equipmentList);
    }
}