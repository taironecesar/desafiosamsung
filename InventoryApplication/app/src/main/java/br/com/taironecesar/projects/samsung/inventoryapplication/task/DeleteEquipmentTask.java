package br.com.taironecesar.projects.samsung.inventoryapplication.task;

import android.os.AsyncTask;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.taironecesar.projects.samsung.inventoryapplication.database.DatabaseClient;
import br.com.taironecesar.projects.samsung.inventoryapplication.model.Equipment;

public class DeleteEquipmentTask extends AsyncTask<Equipment, Void, Void> {
    private ITaskListener listener;

    public DeleteEquipmentTask(ITaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected Void doInBackground(Equipment... equipments) {
        Equipment equipment = equipments[0];

        DatabaseClient.getInstance(listener.getListenerContext()).getInventoryDatabase()
                .equipmentDAO()
                .delete(equipment);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Toast.makeText(listener.getListenerContext(),"Deleted", Toast.LENGTH_LONG).show();
        listener.onSuccess(new ArrayList<Equipment>());
    }
}

