package br.com.taironecesar.projects.samsung.inventoryapplication.task;

import android.content.Context;

import java.util.List;

import br.com.taironecesar.projects.samsung.inventoryapplication.model.Equipment;

public interface ITaskListener {
    Context getListenerContext();
    void onSuccess(List<Equipment> equipmentList);
    void onError();
}
